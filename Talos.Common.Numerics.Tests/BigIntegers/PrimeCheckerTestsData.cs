﻿using System.Collections.Generic;
using System.Numerics;
using Talos.Common.Numerics.BigIntegers.PrimeCheckers;
using Talos.Common.Numerics.BigIntegers.RandomGenerator;

namespace Talos.Common.Numerics.Tests.BigIntegers
{
    public static class PrimeCheckerTestsData
    {
        private static readonly IRandomBigIntegerGenerator randomBigIntegerGenerator = new RandomBigIntegerGenerator();

        public static IEnumerable<object[]> CorrectlyAcceptsNumberAsPrimeData
        {
            get
            {
                yield return new object[]
                {
                    new MillerRabinPrimeChecker(randomBigIntegerGenerator),
                    new BigInteger(37)
                };
            }
        }

        public static IEnumerable<object[]> CorrectlyRejectsNumberAsPrimeData
        {
            get
            {
                yield return new object[]
                {
                    new MillerRabinPrimeChecker(randomBigIntegerGenerator),
                    new BigInteger(36)
                };
            }
        }
    }
}
