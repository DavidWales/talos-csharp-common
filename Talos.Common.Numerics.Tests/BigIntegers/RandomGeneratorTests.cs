﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using Talos.Common.Numerics.BigIntegers.RandomGenerator;

namespace Talos.Common.Numerics.Tests.BigIntegers
{
    [TestClass]
    public class RandomGeneratorTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(RandomGeneratorTestsData.ReturnedNumberInRangeData), typeof(RandomGeneratorTestsData))]
        public void ReturnedNumberInRange(IRandomBigIntegerGenerator randomGenerator, BigInteger min, BigInteger max)
        {
            var random = randomGenerator.Next(min, max);
            random.Should().BeGreaterOrEqualTo(min);
            random.Should().BeLessOrEqualTo(max);
        }

        [TestMethod]
        [DynamicData(nameof(RandomGeneratorTestsData.ReturnedNumberWithBitsData), typeof(RandomGeneratorTestsData))]
        public void ReturnedNumberWithBits(IRandomBigIntegerGenerator randomGenerator, int numBits)
        {
            var random = randomGenerator.Next(numBits);
            random.GetByteCount().Should().Be(numBits / 8);
        }
    }
}
