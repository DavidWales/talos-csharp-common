﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Numerics;
using Talos.Common.Numerics.BigIntegers.PrimeCheckers;

namespace Talos.Common.Numerics.Tests.BigIntegers
{
    [TestClass]
    public class PrimeCheckerTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(PrimeCheckerTestsData.CorrectlyAcceptsNumberAsPrimeData), typeof(PrimeCheckerTestsData))]
        public void CorrectlyAcceptsNumberAsPrime(IPrimeChecker primeChecker, BigInteger n)
        {
            var isPrime = primeChecker.IsPrime(n);
            isPrime.Should().BeTrue();
        }

        [TestMethod]
        [DynamicData(nameof(PrimeCheckerTestsData.CorrectlyRejectsNumberAsPrimeData), typeof(PrimeCheckerTestsData))]
        public void CorrectlyRejectsNumberAsPrime(IPrimeChecker primeChecker, BigInteger n)
        {
            var isPrime = primeChecker.IsPrime(n);
            isPrime.Should().BeFalse();
        }
    }
}
