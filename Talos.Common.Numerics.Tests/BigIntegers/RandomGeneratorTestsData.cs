﻿using System.Collections.Generic;
using System.Numerics;
using Talos.Common.Numerics.BigIntegers.RandomGenerator;

namespace Talos.Common.Numerics.Tests.BigIntegers
{
    public static class RandomGeneratorTestsData
    {
        public static IEnumerable<object[]> ReturnedNumberInRangeData
        {
            get
            {
                yield return new object[]
                {
                    new RandomBigIntegerGenerator(),
                    new BigInteger(10),
                    new BigInteger(20)
                };
            }
        }

        public static IEnumerable<object[]> ReturnedNumberWithBitsData
        {
            get
            {
                yield return new object[]
                {
                    new RandomBigIntegerGenerator(),
                    256
                };
            }
        }
    }
}
