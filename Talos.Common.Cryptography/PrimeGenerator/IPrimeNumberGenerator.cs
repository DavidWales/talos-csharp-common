﻿using System.Numerics;

namespace Talos.Common.Cryptography.PrimeGenerator
{
    public interface IPrimeNumberGenerator
    {

        BigInteger Generate(int bits);

    }
}
