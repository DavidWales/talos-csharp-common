﻿using System.Numerics;
using Talos.Common.Numerics.BigIntegers.PrimeCheckers;
using Talos.Common.Numerics.BigIntegers.RandomGenerator;

namespace Talos.Common.Cryptography.PrimeGenerator
{
    public class PrimeNumberGenerator : IPrimeNumberGenerator
    {
        private readonly IPrimeChecker _primeChecker;
        private readonly IRandomBigIntegerGenerator _randomBigIntegerGenerator;

        public PrimeNumberGenerator(IRandomBigIntegerGenerator randomBigIntegerGenerator,
            IPrimeChecker primeChecker)
        {
            _randomBigIntegerGenerator = randomBigIntegerGenerator;
            _primeChecker = primeChecker;
        }

        public BigInteger Generate(int bits)
        {
            while(true)
            {
                var bigInt = BigInteger.Abs(_randomBigIntegerGenerator.Next(bits));
                if (_primeChecker.IsPrime(bigInt)) return bigInt;
            }
        }
    }
}
