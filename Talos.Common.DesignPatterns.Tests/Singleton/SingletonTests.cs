﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.DesignPatterns.Singleton;

namespace Talos.Common.DesignPatterns.Tests.Singleton
{
    [TestClass]
    public class SingletonTests : BaseTest
    {
        public void SingletonReturnsSameInstance()
        {
            var firstSingletonRef = Singleton<EmptyClass>.GetInstance();
            var secondSingletonRef = Singleton<EmptyClass>.GetInstance();
            firstSingletonRef.Should().Be(secondSingletonRef);
        }
    }
}
