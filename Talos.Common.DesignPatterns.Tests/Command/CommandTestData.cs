﻿using System.Collections.Generic;

namespace Talos.Common.DesignPatterns.Tests.Command
{
    public class CommandTestData
    {
        public static IEnumerable<object[]> ParameterCommandExecutesData
        {
            get
            {
                yield return new object[]
                {
                    4
                };
            }
        }
    }
}
