﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Talos.Common.DesignPatterns.Command;

namespace Talos.Common.DesignPatterns.Tests.Command
{
    [TestClass]
    public class CommandTests : BaseTest
    {
        [TestMethod]
        public void ParameterlessCommandExecutes()
        {
            var command = new Mock<BaseCommand>();
            command.Object.Execute();

            command.Verify(x => x.Execute(), Times.Once);
        }

        [TestMethod]
        [DynamicData(nameof(CommandTestData.ParameterCommandExecutesData), typeof(CommandTestData))]
        public void ParameterCommandExecutes(int number)
        {
            var command = new Mock<BaseCommand<int>>(number);
            command.Object.Execute();

            command.Verify(x => x.Execute(), Times.Once);
            command.Object.Parameter.Should().Be(number);
        }
    }
}
