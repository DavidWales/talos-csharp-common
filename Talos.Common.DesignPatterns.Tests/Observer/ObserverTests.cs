﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Talos.Common.DesignPatterns.Observer;

namespace Talos.Common.DesignPatterns.Tests.Observer
{
    [TestClass]
    public class ObserverTests : BaseTest
    {
        [TestMethod]
        public void CanAttachObserver()
        {
            var observerSubject = new EmptyObserverSubject();
            var mockObserver = new Mock<IObserver<BaseObserverSubject>>();

            var initalObserverCount = observerSubject.ObserverCount();
            observerSubject.AttachObserver(mockObserver.Object);
            observerSubject.ObserverCount().Should().Be(initalObserverCount + 1);
        }

        [TestMethod]
        public void CanDetachObserver()
        {
            var observerSubject = new EmptyObserverSubject();
            var mockObserver = new Mock<IObserver<BaseObserverSubject>>();

            var initalObserverCount = observerSubject.ObserverCount();
            observerSubject.AttachObserver(mockObserver.Object);
            observerSubject.ObserverCount().Should().Be(initalObserverCount + 1);

            observerSubject.DetachObserver(mockObserver.Object);
            observerSubject.ObserverCount().Should().Be(initalObserverCount);
        }

        [TestMethod]
        public void CanNotifyObservers()
        {
            var observerSubject = new EmptyObserverSubject();
            var mockObserver = new Mock<IObserver<BaseObserverSubject>>();

            var initalObserverCount = observerSubject.ObserverCount();
            observerSubject.AttachObserver(mockObserver.Object);
            observerSubject.NotifyObservers();
            mockObserver.Verify(x => x.Handle(observerSubject), Times.Once);
        }
    }
}
