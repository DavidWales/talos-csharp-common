﻿using System.Collections.Generic;
using Talos.Common.DesignPatterns.Memento;

namespace Talos.Common.DesignPatterns.Tests.Memento
{
    public static class MementoTestData
    {
        private static IMemento<string> StringMemento = new BinaryMomento<string>();

        public static IEnumerable<object[]> CanSaveMementoData
        {
            get
            {
                yield return new object[]
                {
                    StringMemento,
                    "Test"
                };
            }
        }

        public static IEnumerable<object[]> CanRestoreMementoData
        {
            get
            {
                yield return new object[]
                {
                    StringMemento,
                    "Test1",
                    "Test2"
                };
            }
        }
    }
}
