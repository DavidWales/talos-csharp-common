﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.DesignPatterns.Memento;

namespace Talos.Common.DesignPatterns.Tests.Memento
{
    [TestClass]
    public class MementoTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(MementoTestData.CanSaveMementoData), typeof(MementoTestData))]
        public void CanSaveMemento(IMemento<string> memento, string state)
        {
            memento.State = state;
            memento.SaveToMemento();

            memento.SavedState.Should().Be(state);
        }

        [TestMethod]
        [DynamicData(nameof(MementoTestData.CanRestoreMementoData), typeof(MementoTestData))]
        public void CanRestoreMemento(IMemento<string> memento, string firstState, string secondState)
        {
            memento.State = firstState;
            memento.SaveToMemento();

            memento.State = secondState;
            memento.State.Should().Be(secondState);

            memento.RestoreFromMemento();
            memento.State.Should().Be(firstState);
        }
    }
}
