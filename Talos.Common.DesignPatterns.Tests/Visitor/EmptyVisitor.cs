﻿using Talos.Common.DesignPatterns.Visitor;

namespace Talos.Common.DesignPatterns.Tests.Visitor
{
    public class EmptyVisitor : IVisitor<Acceptor>
    {
        public virtual void Visit(Acceptor obj)
        {
            // Do Something
        }
    }
}
