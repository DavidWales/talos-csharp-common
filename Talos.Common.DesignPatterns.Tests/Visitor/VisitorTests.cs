﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Talos.Common.DesignPatterns.Visitor;

namespace Talos.Common.DesignPatterns.Tests.Visitor
{
    [TestClass]
    public class VisitorTests : BaseTest
    {
        [TestMethod]
        public void VisitsWithConcreteType()
        {
            var acceptor = new Mock<Acceptor>();
            var visitor = new Mock<EmptyVisitor>();

            acceptor.Object.Accept(visitor.Object);
            visitor.Verify(x => x.Visit(acceptor.Object), Times.Once);
        }
    }
}
