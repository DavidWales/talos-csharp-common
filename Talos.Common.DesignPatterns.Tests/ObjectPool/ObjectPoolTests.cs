﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.DesignPatterns.ObjectPool;

namespace Talos.Common.DesignPatterns.Tests.ObjectPool
{
    [TestClass]
    public class ObjectPoolTests : BaseTest
    {
        [TestMethod]
        public void ConstructorCreatesEmptyPool()
        {
            var objectPool = new ObjectPool<EmptyClass>();
            objectPool.Count().Should().Be(0);
        }

        [TestMethod]
        [DynamicData(nameof(ObjectPoolTestData.ConstructorWarmsUpObjectPoolData), typeof(ObjectPoolTestData))]
        public void ConstructorWarmsUpObjectPool(int count)
        {
            var objectPool = new ObjectPool<EmptyClass>(count);
            objectPool.Count().Should().Be(count);
        }

        [TestMethod]
        [DynamicData(nameof(ObjectPoolTestData.WarmUpCreatesInstancesData), typeof(ObjectPoolTestData))]
        public void WarmUpCreatesInstances(IObjectPool<EmptyClass> objectPool, int count)
        {
            objectPool.Count().Should().Be(0);
            objectPool.WarmUp(count);
            objectPool.Count().Should().Be(count);
        }

        [TestMethod]
        [DynamicData(nameof(ObjectPoolTestData.CreateGrabsFromPoolData), typeof(ObjectPoolTestData))]
        public void CreateGrabsFromPool(IObjectPool<EmptyClass> objectPool)
        {
            objectPool.WarmUp(1);
            var initialCount = objectPool.Count();
            var obj = objectPool.Create();
            obj.Should().NotBeNull();
            objectPool.Count().Should().Be(initialCount - 1);
        }

        [TestMethod]
        [DynamicData(nameof(ObjectPoolTestData.CreateInitializesNewInstanceData), typeof(ObjectPoolTestData))]
        public void CreateInitializesNewInstance(IObjectPool<EmptyClass> objectPool)
        {
            objectPool.Count().Should().Be(0);
            var obj = objectPool.Create();
            obj.Should().NotBeNull();
        }

        [TestMethod]
        [DynamicData(nameof(ObjectPoolTestData.DestroyAddsToPoolData), typeof(ObjectPoolTestData))]
        public void DestroyAddsToPool(IObjectPool<EmptyClass> objectPool)
        {
            var initialCount = objectPool.Count();
            var obj = objectPool.Create();
            objectPool.Destroy(obj);
            objectPool.Count().Should().Be(initialCount + 1);
        }
    }
}
