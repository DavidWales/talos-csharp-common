﻿using System.Collections.Generic;
using Talos.Common.DesignPatterns.ObjectPool;

namespace Talos.Common.DesignPatterns.Tests.ObjectPool
{
    public static class ObjectPoolTestData
    {
        public static IEnumerable<object[]> ConstructorWarmsUpObjectPoolData
        {
            get
            {
                yield return new object[]
                {
                    2
                };
            }
        }

        public static IEnumerable<object[]> WarmUpCreatesInstancesData
        {
            get
            {
                yield return new object[]
                {
                    new ObjectPool<EmptyClass>(),
                    2
                };
            }
        }

        public static IEnumerable<object[]> CreateGrabsFromPoolData
        {
            get
            {
                yield return new object[]
                {
                    new ObjectPool<EmptyClass>()
                };
            }
        }

        public static IEnumerable<object[]> CreateInitializesNewInstanceData
        {
            get
            {
                yield return new object[]
                {
                    new ObjectPool<EmptyClass>()
                };
            }
        }

        public static IEnumerable<object[]> DestroyAddsToPoolData
        {
            get
            {
                yield return new object[]
                {
                    new ObjectPool<EmptyClass>()
                };
            }
        }
    }
}
