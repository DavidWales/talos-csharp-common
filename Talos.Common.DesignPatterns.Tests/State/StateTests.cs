﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Talos.Common.DesignPatterns.State;

namespace Talos.Common.DesignPatterns.Tests.State
{
    [TestClass]
    public class StateTests : BaseTest
    {
        [TestMethod]
        public void ExecutesParameterlessState()
        {
            var state = new Mock<IState>();
            state.SetupSet(x => x.Context = It.IsAny<IStateContext>()).Verifiable();
            var stateContext = new StateContext { State = state.Object };

            stateContext.Execute();
            state.Verify(x => x.Execute(), Times.Once);
            state.Verify();
        }

        [TestMethod]
        [DynamicData(nameof(StateTestData.ExecutesParameterStateData), typeof(StateTestData))]
        public void ExecutesParameterState(int number)
        {
            var state = new Mock<IState<int>>();
            state.SetupSet(x => x.Context = It.IsAny<IStateContext<int>>()).Verifiable();
            var stateContext = new StateContext<int> { State = state.Object };

            stateContext.Execute(number);
            state.Verify(x => x.Execute(number), Times.Once);
            state.Verify();
        }

        [TestMethod]
        public void ExecutesReassignedParameterlessState()
        {
            var firstState = new Mock<IState>();
            var secondState = new Mock<IState>();
            secondState.SetupSet(x => x.Context = It.IsAny<IStateContext>()).Verifiable();

            var stateContext = new StateContext() { State = firstState.Object };
            stateContext.State.Should().Be(firstState.Object);

            stateContext.State = secondState.Object;
            stateContext.State.Should().Be(secondState.Object);

            stateContext.Execute();
            firstState.Verify(x => x.Execute(), Times.Never);
            secondState.Verify(x => x.Execute(), Times.Once);
            secondState.Verify();
        }

        [TestMethod]
        [DynamicData(nameof(StateTestData.ExecutesReassignedParameterStateData), typeof(StateTestData))]
        public void ExecutesReassignedParameterState(int number)
        {
            var firstState = new Mock<IState<int>>();
            var secondState = new Mock<IState<int>>();
            secondState.SetupSet(x => x.Context = It.IsAny<IStateContext<int>>()).Verifiable();

            var stateContext = new StateContext<int>() { State = firstState.Object };
            stateContext.State.Should().Be(firstState.Object);

            stateContext.State = secondState.Object;
            stateContext.State.Should().Be(secondState.Object);

            stateContext.Execute(number);
            firstState.Verify(x => x.Execute(number), Times.Never);
            secondState.Verify(x => x.Execute(number), Times.Once);
            secondState.Verify();
        }
    }
}
