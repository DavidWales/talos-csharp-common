﻿using System.Collections.Generic;

namespace Talos.Common.DesignPatterns.Tests.State
{
    public static class StateTestData
    {
        public static IEnumerable<object[]> ExecutesParameterStateData
        {
            get
            {
                yield return new object[]
                {
                    5
                };
            }
        }

        public static IEnumerable<object[]> ExecutesReassignedParameterStateData
        {
            get
            {
                yield return new object[]
                {
                    5
                };
            }
        }
    }
}
