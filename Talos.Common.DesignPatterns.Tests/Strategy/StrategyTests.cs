﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Talos.Common.DesignPatterns.Strategy;

namespace Talos.Common.DesignPatterns.Tests.Strategy
{
    [TestClass]
    public class StrategyTests : BaseTest
    {
        [TestMethod]
        public void ExecutesEmptyStrategy()
        {
            var strategy = new Mock<IStrategy>();
            var strategyContext = new StrategyContext(strategy.Object);

            strategyContext.Execute();
            strategy.Verify(x => x.Execute(), Times.Once);
        }

        [TestMethod]
        [DynamicData(nameof(StrategyTestData.ExecutesParameterStrategyData), typeof(StrategyTestData))]
        public void ExecutesParameterStrategy(int number)
        {
            var strategy = new Mock<IStrategy<int>>();
            var strategyContext = new StrategyContext<int>(strategy.Object);

            strategyContext.Execute(number);
            strategy.Verify(x => x.Execute(number), Times.Once);
        }

        [TestMethod]
        [DynamicData(nameof(StrategyTestData.ExecutesReturnParameterStrategy), typeof(StrategyTestData))]
        public void ExecutesReturnParameterStrategy(int number, int expectedResult)
        {
            var strategy = new Mock<IStrategy<int, int>>();
            strategy.Setup(x => x.Execute(It.Is<int>(p => p == number))).Returns(expectedResult);

            var strategyContext = new StrategyContext<int, int>(strategy.Object);

            var actualResult = strategyContext.Execute(number);
            actualResult.Should().Be(expectedResult);
            strategy.Verify(x => x.Execute(number), Times.Once);
        }
    }
}
