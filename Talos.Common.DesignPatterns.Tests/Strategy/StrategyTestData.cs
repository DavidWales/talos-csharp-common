﻿using System.Collections.Generic;

namespace Talos.Common.DesignPatterns.Tests.Strategy
{
    public static class StrategyTestData
    {
        public static IEnumerable<object[]> ExecutesParameterStrategyData
        {
            get
            {
                yield return new object[]
                {
                    10
                };
            }
        }

        public static IEnumerable<object[]> ExecutesReturnParameterStrategy
        {
            get
            {
                yield return new object[]
                {
                    10,
                    20
                };
            }
        }
    }
}
