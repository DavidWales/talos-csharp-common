﻿using Moq;
using System.Collections.Generic;
using System.Numerics;
using Talos.Common.Cryptography.PrimeGenerator;
using Talos.Common.Numerics.BigIntegers.PrimeCheckers;
using Talos.Common.Numerics.BigIntegers.RandomGenerator;

namespace Talos.Common.Cryptography.Tests.PrimeGeneratorTests
{
    public static class PrimeGeneratorTestsData
    {
        public static IEnumerable<object[]> GeneratesNumberWithCorrectBitsData
        {
            get
            {
                var mockRandomBigIntGenerator = new Mock<IRandomBigIntegerGenerator>();
                mockRandomBigIntGenerator.Setup(x => x.Next(It.IsAny<int>())).Returns(13);

                var mockPrimalityChecker = new Mock<IPrimeChecker>();
                mockPrimalityChecker.Setup(x => x.IsPrime(It.IsAny<BigInteger>())).Returns(true);

                yield return new object[]
                {
                    new PrimeNumberGenerator(mockRandomBigIntGenerator.Object, mockPrimalityChecker.Object),
                    8
                };
            }
        }

    }
}
