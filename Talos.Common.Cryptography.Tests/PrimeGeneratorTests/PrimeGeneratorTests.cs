﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Cryptography.PrimeGenerator;

namespace Talos.Common.Cryptography.Tests.PrimeGeneratorTests
{
    [TestClass]
    public class SimplePrimeGeneratorTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(PrimeGeneratorTestsData.GeneratesNumberWithCorrectBitsData), typeof(PrimeGeneratorTestsData))]
        public void GeneratesNumberWithCorrectBits(IPrimeNumberGenerator primeNumberGenerator, int bits)
        {
            var prime = primeNumberGenerator.Generate(bits);
            prime.GetByteCount().Should().Be(bits / 8);
        }
    }
}
