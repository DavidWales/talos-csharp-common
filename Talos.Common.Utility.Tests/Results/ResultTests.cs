﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Results;

namespace Talos.Common.Utility.Tests.Results
{
    [TestClass]
    public class ResultTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(ResultTestData.MatchesWithNoResultData), typeof(ResultTestData))]
        public void MatchesWithNoResult(Result<string> result, string expected)
        {
            string actual = null;
            result.Match(
                Ok: x => actual = x,
                Error: error => actual = error
            );

            actual.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        [DynamicData(nameof(ResultTestData.MatchesWithResultData), typeof(ResultTestData))]
        public void MatchesWithResult(Result<string> result, string expected)
        {
            var actual = result.Match(
                Ok: data => data,
                Error: error => error
            );
            actual.Should().BeEquivalentTo(expected);
        }

        [TestMethod]
        [DynamicData(nameof(ResultTestData.UnwrapMatchesExpectedData), typeof(ResultTestData))]
        public void UnwrapMatchesExpected(Result<string> result, string def, string expected)
        {
            var actual = result.UnwrapOr(def);
            actual.Should().BeEquivalentTo(expected);
        }
    }
}
