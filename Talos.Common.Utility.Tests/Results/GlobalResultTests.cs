﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Results;

namespace Talos.Common.Utility.Tests.Results
{
    [TestClass]
    public class GlobalResultTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(GlobalResultTestData.CreatesOkResultData), typeof(GlobalResultTestData))]
        public void CreatesOkResult(string data)
        {
            var result = Result.Ok(data);
            result.Should().NotBeNull();
            result.IsOk().Should().BeTrue();
        }

        [TestMethod]
        [DynamicData(nameof(GlobalResultTestData.CreatesErrorResultData), typeof(GlobalResultTestData))]
        public void CreatesErroResult(string errorCode)
        {
            var result = Result.Error<string>(errorCode);
            result.Should().NotBeNull();
            result.IsError().Should().BeTrue();
        }
    }
}
