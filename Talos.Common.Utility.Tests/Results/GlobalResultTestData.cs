﻿using System.Collections.Generic;

namespace Talos.Common.Utility.Tests.Results
{
    public static class GlobalResultTestData
    {
        public static IEnumerable<object[]> CreatesOkResultData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }

        public static IEnumerable<object[]> CreatesErrorResultData
        {
            get
            {
                yield return new object[]
                {
                    "400"
                };
            }
        }
    }
}
