﻿using System.Collections.Generic;
using Talos.Common.Utility.Results;

namespace Talos.Common.Utility.Tests.Results
{
    public static class ResultTestData
    {
        public static IEnumerable<object[]> MatchesWithNoResultData
        {
            get
            {
                yield return new object[]
                {
                    Result.Ok("test"),
                    "test"
                };

                yield return new object[]
                {
                    Result.Error<string>("400"),
                    "400"
                };
            }
        }

        public static IEnumerable<object[]> MatchesWithResultData
        {
            get
            {
                yield return new object[]
                {
                    Result.Ok("test"),
                    "test"
                };

                yield return new object[]
                {
                    Result.Error<string>("400"),
                    "400"
                };
            }
        }

        public static IEnumerable<object[]> UnwrapMatchesExpectedData
        {
            get
            {
                yield return new object[]
                {
                    Result.Ok("test"),
                    null,
                    "test"
                };

                yield return new object[]
                {
                    Result.Error<string>("400"),
                    "test",
                    "test"
                };
            }
        }
    }
}
