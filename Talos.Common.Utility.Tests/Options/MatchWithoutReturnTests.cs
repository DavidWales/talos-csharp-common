﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Options;

namespace Talos.Common.Utility.Tests.Options
{
    [TestClass]
    public class MatchWithoutReturnTests : BaseTest
    {
        [TestMethod]
        public void MatchesSome()
        {
            var someOption = Option.Some(string.Empty);
            var isSome = false;

            someOption.Match(
                Some: x => {
                    isSome = true;
                },
                None: () =>
                {
                    isSome = false;
                }
            );

            isSome.Should().BeTrue();
        }

        [TestMethod]
        public void MatchesNone()
        {
            var someOption = Option.None<string>();
            var isNone = false;

            someOption.Match(
                Some: x => {
                    isNone = false;
                },
                None: () =>
                {
                    isNone = true;
                }
            );

            isNone.Should().BeTrue();
        }
    }
}
