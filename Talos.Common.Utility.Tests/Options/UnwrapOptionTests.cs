﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Options;

namespace Talos.Common.Utility.Tests.Options
{
    [TestClass]
    public class UnwrapOptionTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(OptionTestsData.UnwrapShouldYieldInitialData), typeof(OptionTestsData))]
        public void UnwrapShouldYieldInitial(string someValue)
        {
            var someOption = Option.Some(new string(someValue));
            someOption.UnwrapOr(string.Empty).Should().Be(new string(someValue));
        }

        [TestMethod]
        [DynamicData(nameof(OptionTestsData.UnwrapShouldYieldOrData), typeof(OptionTestsData))]
        public void UnwrapShouldYieldOr(string someValue)
        {
            var noneOption = Option.None<string>();
            noneOption.UnwrapOr(new string(someValue)).Should().Be(new string(someValue));
        }
    }
}
