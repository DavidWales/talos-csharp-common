﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Options;

namespace Talos.Common.Utility.Tests.Options
{
    [TestClass]
    public class MatchWithReturnTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(OptionTestsData.MatchSomeOptionReturnsExpectedData), typeof(OptionTestsData))]
        public void MatchSomeOptionReturnsExpected(string someValue)
        {
            var someOption = Option.Some(someValue);
            var result = someOption.Match(
                Some: x => x + "1"
            );

            result.Should().Be(someValue + "1");
        }

        [TestMethod]
        public void MatchNoneOptionReturnsExpected()
        {
            var noneOption = Option.None<string>();
            var result = noneOption.Match(
                None: () => string.Empty
            );

            result.Should().Be(string.Empty);
        }
    }
}
