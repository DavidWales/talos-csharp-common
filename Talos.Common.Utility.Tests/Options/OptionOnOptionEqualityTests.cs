﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Options;

namespace Talos.Common.Utility.Tests.Options
{
    [TestClass]
    public class OptionOnOptionEqualityTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(OptionTestsData.SomeOptionShouldEqualSomeOptionData), typeof(OptionTestsData))]
        public void SomeOptionShouldEqualSomeOption(string someValue)
        {
            var firstOption = Option.Some(new string(someValue));
            var secondOption = Option.Some(new string(someValue));
            firstOption.Equals(secondOption).Should().BeTrue();
            secondOption.Equals(firstOption).Should().BeTrue();
        }

        [TestMethod]
        public void NoneOptionShouldEqualNoneOption()
        {
            var firstOption = Option.None<string>();
            var secondOption = Option.None<string>();
            firstOption.Equals(secondOption).Should().BeTrue();
            secondOption.Equals(firstOption).Should().BeTrue();
        }

        [TestMethod]
        [DynamicData(nameof(OptionTestsData.SomeOptionShouldNotEqualSomeOptionData), typeof(OptionTestsData))]
        public void SomeOptionShouldNotEqualSomeOption(string firstOptionValue, string secondOptionValue)
        {
            var firstOption = Option.Some(firstOptionValue);
            var secondOption = Option.Some(secondOptionValue);
            firstOption.Equals(secondOption).Should().BeFalse();
            secondOption.Equals(firstOption).Should().BeFalse();
        }

        [TestMethod]
        [DynamicData(nameof(OptionTestsData.NoneOptionShouldNotEqualSomeOptionData), typeof(OptionTestsData))]
        public void NoneOptionShouldNotEqualSomeOption(string someValue)
        {
            var firstOption = Option.None<string>();
            var secondOption = Option.Some(someValue);
            firstOption.Equals(secondOption).Should().BeFalse();
            secondOption.Equals(firstOption).Should().BeFalse();
        }
    }
}
