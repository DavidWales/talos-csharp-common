﻿using System.Collections.Generic;

namespace Talos.Common.Utility.Tests.Options
{
    public static class OptionTestsData
    {
        public static IEnumerable<object[]> IsNoneShouldBeFalseWithSomeData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }

        public static IEnumerable<object[]> IsSomeShouldBeTrueWithSomeData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }

        public static IEnumerable<object[]> SomeOptionShouldEqualSomeOptionData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }

        public static IEnumerable<object[]> SomeOptionShouldNotEqualSomeOptionData
        {
            get
            {
                yield return new object[]
                {
                    "test1",
                    "test2"
                };
            }
        }

        public static IEnumerable<object[]> NoneOptionShouldNotEqualSomeOptionData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }

        public static IEnumerable<object[]> SomeOptionShouldEqualTypeData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }

        public static IEnumerable<object[]> SomeOptionShouldNotEqualTypeData
        {
            get
            {
                yield return new object[]
                {
                    "test1",
                    "test2"
                };
            }
        }

        public static IEnumerable<object[]> NoneOptionShouldNotEqualTypeData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }

        public static IEnumerable<object[]> UnwrapShouldYieldInitialData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }

        public static IEnumerable<object[]> UnwrapShouldYieldOrData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }
        
        public static IEnumerable<object[]> MatchSomeOptionReturnsExpectedData
        {
            get
            {
                yield return new object[]
                {
                    "test"
                };
            }
        }
    }
}
