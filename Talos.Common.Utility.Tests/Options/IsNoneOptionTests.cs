﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Options;

namespace Talos.Common.Utility.Tests.Options
{
    [TestClass]
    public class IsNoneOptionTests : BaseTest
    {
        [TestMethod]
        public void IsNoneShouldBeTrueWithNone()
        {
            var noneOption = Option.None<string>();
            noneOption.IsNone().Should().BeTrue();
        }

        [TestMethod]
        [DynamicData(nameof(OptionTestsData.IsNoneShouldBeFalseWithSomeData), typeof(OptionTestsData))]
        public void IsNoneShouldBeFalseWithSome(string someValue)
        {
            var someOption = Option.Some(someValue);
            someOption.IsNone().Should().BeFalse();
        }

        [TestMethod]
        public void IsNoneShouldBeTrueWithSome()
        {
            var someOption = Option.Some<string>(null);
            someOption.IsNone().Should().BeTrue();
        }
    }
}
