﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Options;

namespace Talos.Common.Utility.Tests.Options
{
    [TestClass]
    public class OptionOnTypeEqualityTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(OptionTestsData.SomeOptionShouldEqualTypeData), typeof(OptionTestsData))]
        public void SomeOptionShouldEqualType(string someValue)
        {
            var someOption = Option.Some(new string(someValue));
            var someString = new string(someValue);
            someOption.Equals(someString).Should().BeTrue();
        }

        [TestMethod]
        [DynamicData(nameof(OptionTestsData.SomeOptionShouldNotEqualTypeData), typeof(OptionTestsData))]
        public void SomeOptionShouldNotEqualType(string firstValue, string secondValue)
        {
            var someOption = Option.Some(firstValue);
            var someString = secondValue;
            someOption.Equals(someString).Should().BeFalse();
        }

        [TestMethod]
        [DynamicData(nameof(OptionTestsData.NoneOptionShouldNotEqualTypeData), typeof(OptionTestsData))]
        public void NoneOptionShouldNotEqualType(string someValue)
        {
            var noneOption = Option.None<string>();
            var someString = someValue;
            noneOption.Equals(someString).Should().BeFalse();
        }
    }
}
