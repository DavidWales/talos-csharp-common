﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Options;

namespace Talos.Common.Utility.Tests.Options
{
    [TestClass]
    public class IsSomeOptionTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(OptionTestsData.IsSomeShouldBeTrueWithSomeData), typeof(OptionTestsData))]
        public void IsSomeShouldBeTrueWithSome(string someValue)
        {
            var someOption = Option.Some(someValue);
            someOption.IsSome().Should().BeTrue();
        }

        [TestMethod]
        public void IsSomeShouldBeFalseWithSome()
        {
            var someOption = Option.Some<string>(null);
            someOption.IsSome().Should().BeFalse();
        }

        [TestMethod]
        public void IsSomeShouldBeFalseWithNone()
        {
            var noneOption = Option.None<string>();
            noneOption.IsSome().Should().BeFalse();
        }
    }
}
