﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.Utility.Chainmail;
using Talos.Common.Utility.Tests.Chainlink.ExampleClasses;

namespace Talos.Common.Utility.Tests.Chainlink
{
    [TestClass]
    public class ChainlinkTests : BaseTest
    {
        [TestMethod]
        public void BuildsChainlink()
        {
            var chainlink = new ChainlinkBuilder<ChainlinkContext>()
                .WithLink(new AuthenticationLink())
                .WithLink(new HashAuthenticationLink())
                .WithLink(new LocationLink())
                .Build();

            chainlink.Should().NotBeNull();
        }

        [TestMethod]
        [DynamicData(nameof(ChainlinkTestData.ChainlinkRunsData), typeof(ChainlinkTestData))]
        public void ChainlinkRuns(IChainlink<ChainlinkContext> chainlink, ChainlinkContext context,
            ChainlinkContext expectedResultContext)
        {
            chainlink.Run(context);
            context.Should().BeEquivalentTo(expectedResultContext);
        }
    }
}
