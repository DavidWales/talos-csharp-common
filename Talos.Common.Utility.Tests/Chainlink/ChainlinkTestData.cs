﻿using System.Collections.Generic;
using Talos.Common.Utility.Chainmail;
using Talos.Common.Utility.Tests.Chainlink.ExampleClasses;

namespace Talos.Common.Utility.Tests.Chainlink
{
    public static class ChainlinkTestData
    {
        public static IEnumerable<object[]> ChainlinkRunsData
        {
            get
            {
                var chainlink = new ChainlinkBuilder<ChainlinkContext>()
                    .WithLink(new AuthenticationLink())
                    .WithLink(new HashAuthenticationLink())
                    .WithLink(new LocationLink())
                    .Build();

                yield return new object[]
                {
                    chainlink,
                    new ChainlinkContext
                    {
                        Username = "david.wales",
                        Password = "abcd123",
                        Latitude = 41.8781f,
                        Longitude = 87.6298f
                    },
                    new ChainlinkContext
                    {
                        Username = "david.wales".GetHashCode().ToString(),
                        Password = "abcd123".GetHashCode().ToString(),
                        Latitude = 41.8781f,
                        Longitude = 87.6298f
                    }
                };
            }
        }
    }
}
