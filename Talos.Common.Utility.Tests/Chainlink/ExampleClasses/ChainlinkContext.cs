﻿namespace Talos.Common.Utility.Tests.Chainlink.ExampleClasses
{
    public class ChainlinkContext : IAuthenticationContext, ILocationContext
    {
        public float Latitude { get; set; }

        public float Longitude { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
