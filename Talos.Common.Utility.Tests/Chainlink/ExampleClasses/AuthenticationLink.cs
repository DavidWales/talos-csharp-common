﻿using Talos.Common.Utility.Chainmail;

namespace Talos.Common.Utility.Tests.Chainlink.ExampleClasses
{
    public class AuthenticationLink : ILink<IAuthenticationContext>
    {
        public void Execute(IAuthenticationContext context)
        {
            // Modify Context
        }
    }
}
