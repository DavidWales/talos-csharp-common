﻿namespace Talos.Common.Utility.Tests.Chainlink.ExampleClasses
{
    public interface IAuthenticationContext
    {

        string Username { get; set; }

        string Password { get; set; }

    }
}
