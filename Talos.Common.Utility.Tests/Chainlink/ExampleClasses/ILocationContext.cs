﻿namespace Talos.Common.Utility.Tests.Chainlink.ExampleClasses
{
    public interface ILocationContext
    {

        float Latitude { get; set; }

        float Longitude { get; set; }

    }
}
