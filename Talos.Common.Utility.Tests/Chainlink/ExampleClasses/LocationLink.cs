﻿using Talos.Common.Utility.Chainmail;

namespace Talos.Common.Utility.Tests.Chainlink.ExampleClasses
{
    public class LocationLink : ILink<ILocationContext>
    {
        public void Execute(ILocationContext context)
        {
            // Modify context
        }
    }
}
