﻿using Talos.Common.Utility.Chainmail;

namespace Talos.Common.Utility.Tests.Chainlink.ExampleClasses
{
    public class HashAuthenticationLink : ILink<IAuthenticationContext>
    {
        public void Execute(IAuthenticationContext context)
        {
            context.Username = context.Username.GetHashCode().ToString();
            context.Password = context.Password.GetHashCode().ToString();
        }
    }
}
