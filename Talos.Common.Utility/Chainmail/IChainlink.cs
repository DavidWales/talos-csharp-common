﻿namespace Talos.Common.Utility.Chainmail
{
    public interface IChainlink<in T>
    {
        void Run(T context);
    }
}
