﻿using System.Collections.Generic;
using System.Linq;

namespace Talos.Common.Utility.Chainmail
{
    public class Chainlink<T> : IChainlink<T>
    {
        private readonly List<ILink<T>> _links;

        public Chainlink(IEnumerable<ILink<T>> links)
        {
            _links = links.ToList();
        }

        public void Run(T context)
        {
            foreach(var link in _links)
            {
                link.Execute(context);
            }
        }
    }
}
