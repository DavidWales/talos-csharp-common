﻿using System.Collections.Generic;

namespace Talos.Common.Utility.Chainmail
{
    public class ChainlinkBuilder<T> : IChainlinkBuilder<T>
    {
        private readonly IList<ILink<T>> _links;

        public ChainlinkBuilder()
        {
            _links = new List<ILink<T>>();
        }

        public IChainlinkBuilder<T> WithLink(ILink<T> link)
        {
            _links.Add(link);
            return this;
        }

        public IChainlink<T> Build()
        {
            return new Chainlink<T>(_links);
        }
    }
}
