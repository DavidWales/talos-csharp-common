﻿namespace Talos.Common.Utility.Chainmail
{
    public interface IChainlinkBuilder<T>
    {
        IChainlinkBuilder<T> WithLink(ILink<T> link);
        IChainlink<T> Build();
    }
}
