﻿namespace Talos.Common.Utility.Chainmail
{
    public interface ILink<in T>
    {
        void Execute(T context);
    }
}
