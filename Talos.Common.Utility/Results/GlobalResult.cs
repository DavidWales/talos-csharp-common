﻿namespace Talos.Common.Utility.Results
{
    public static class Result
    {

        public static Result<T> Ok<T>(T data)
        {
            return new Result<T>(data);
        }

        public static Result<T> Error<T>(string error)
        {
            return new Result<T>(error);
        }

    }
}
