﻿using System;

namespace Talos.Common.Utility.Results
{
    public class Result<T>
    {

        private T _data;
        private string _error;
        private bool _isError;

        public Result(T data)
        {
            _data = data;
            _isError = false;
        }

        public Result(string error)
        {
            _error = error;
            _isError = true;
        }

        public void Match(Action<T> Ok = null, Action<string> Error = null)
        {
            if(!_isError && Ok != null)
            {
                Ok(_data);
            }
            else if(_isError && Error != null)
            {
                Error(_error);
            }
        }

        public TResult Match<TResult>(Func<T, TResult> Ok = null, Func<string, TResult> Error = null)
        {
            if(!_isError && Ok != null)
            {
                return Ok(_data);
            }
            else if(_isError && Error != null)
            {
                return Error(_error);
            }
            return default;
        }

        public bool IsOk()
        {
            return !_isError;
        }

        public bool IsError()
        {
            return _isError;
        }

        public T UnwrapOr(T defaultValue)
        {
            return _isError ? defaultValue : _data;
        }
    }
}
