﻿using System;

namespace Talos.Common.Utility.Options
{
    public class Option<T>: IEquatable<T>, IEquatable<Option<T>> where T: class
    {
        private readonly T _data;
        private readonly bool _hasData;

        public Option(T data)
        {
            _data = data;
            _hasData = data != null;
        }

        public void Match(Action<T> Some = null, Action None = null)
        {
            if(_hasData && Some != null)
            {
                Some(_data);
            }
            else if(!_hasData && None != null)
            {
                None();
            }
        }

        public TResult Match<TResult>(Func<T, TResult> Some = null, Func<TResult> None = null)
        {
            if(_hasData)
            {
                return Some != null ? Some(_data) : default;
            }
            return None != null ? None() : default;
        }

        public bool IsSome()
        {
            return _hasData;
        }

        public bool IsNone()
        {
            return !_hasData;
        }

        public T UnwrapOr(T value)
        {
            return _hasData ? _data : value;
        }

        public bool Equals(T other)
        {
            if (IsNone() && other is null) return true;
            if (IsNone()) return false; 
            return _data.Equals(other);
        }

        public bool Equals(Option<T> other)
        {
            if (IsNone() && other.IsNone()) return true;
            if (IsNone()) return false;
            return _data.Equals(other._data);
        }
    }
}
