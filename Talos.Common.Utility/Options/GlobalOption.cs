﻿namespace Talos.Common.Utility.Options
{
    public static class Option
    {

        public static Option<T> Some<T>(T data) where T: class
        {
            return new Option<T>(data);
        }

        public static Option<T> None<T>() where T: class
        {
            return new Option<T>(null);
        }

    }
}
