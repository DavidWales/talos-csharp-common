﻿using System;

namespace Talos.Common.State.DependencyInjection
{
    public interface IContainer
    {
        void Add<T>(T obj);

        void Add<T>(Func<IProvider, T> x);

        void AddSingleton<T>(T obj);

        void AddSingleton<T>(Func<IProvider, T> x);

        int Count<T>();
    }
}
