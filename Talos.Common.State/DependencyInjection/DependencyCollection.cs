﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Talos.Common.State.DependencyInjection
{
    public class DependencyCollection<T> : IDependencyCollection
    {
        public Type Type { get; } = typeof(T);

        private readonly ICollection<Dependency<T>> _dependencies;

        public DependencyCollection()
        {
            _dependencies = new List<Dependency<T>>();
        }

        public void Register(Func<IProvider, T> dataResolver, bool isSingleton = false)
        {
            if (isSingleton) _dependencies.Clear();
            _dependencies.Add(new Dependency<T>(dataResolver));
        }

        public T ResolveSingle(IProvider provider)
        {
            if (_dependencies is null) return default;

            var dependency = _dependencies.FirstOrDefault();
            if (dependency is null) return default;

            return dependency.Resolve(provider);
        }

        public List<T> Resolve(IProvider provider)
        {
            var resolvedData = new List<T>();
            if (_dependencies is null) return resolvedData;

            foreach(var dep in _dependencies)
            {
                resolvedData.Add(dep.Resolve(provider));
            }

            return resolvedData;
        }

        public int Count()
        {
            if (_dependencies is null) return 0;
            return _dependencies.Count;
        }
    }
}
