﻿using System;

namespace Talos.Common.State.DependencyInjection
{
    public class Dependency<T>
    {
        private Func<IProvider, T> _dataResolver;

        public Dependency(Func<IProvider, T> dataResolver)
        {
            _dataResolver = dataResolver;
        }

        public T Resolve(IProvider provider)
        {
            return _dataResolver(provider);
        }
    }
}
