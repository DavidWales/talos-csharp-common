﻿using System.Collections.Generic;
using Talos.Common.Utility.Options;

namespace Talos.Common.State.DependencyInjection
{
    public interface IProvider
    {

        Option<List<T>> Get<T>();

        Option<T> GetSingletonReference<T>() where T : class;

        T GetSingletonValue<T>() where T : struct;

    }
}
