﻿using System.Collections.Generic;
using System.Linq;
using Talos.Common.Utility.Options;

namespace Talos.Common.State.DependencyInjection
{
    public class Provider : IProvider
    {
        private readonly IList<IDependencyCollection> _dependencyCollections;

        public Provider(IList<IDependencyCollection> dependencyCollections)
        {
            _dependencyCollections = dependencyCollections;
        }

        public Option<List<T>> Get<T>()
        {
            var collectionOption = GetTypedCollection<T>();
            return collectionOption.Match(
                Some: collection => Option.Some(collection.Resolve(this)),
                None: () => Option.None<List<T>>()
            );
        }

        public Option<T> GetSingletonReference<T>() where T : class
        {
            var collectionOption = GetTypedCollection<T>();
            return collectionOption.Match(
                Some: collection => Option.Some(collection.ResolveSingle(this)),
                None: () => Option.None<T>()
            );
        }

        public T GetSingletonValue<T>() where T : struct
        {
            var collectionOption = GetTypedCollection<T>();
            return collectionOption.Match(
                Some: collection => collection.ResolveSingle(this),
                None: () => default
            );
        }

        private Option<DependencyCollection<T>> GetTypedCollection<T>()
        {
            var collection = _dependencyCollections.FirstOrDefault(x => x.Type == typeof(T));
            if (collection is null) return Option.None<DependencyCollection<T>>();

            return Option.Some((DependencyCollection<T>)collection);
        }
    }
}
