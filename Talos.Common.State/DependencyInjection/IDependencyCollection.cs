﻿using System;

namespace Talos.Common.State.DependencyInjection
{
    public interface IDependencyCollection
    {
        Type Type { get; }

        int Count();
    }
}
