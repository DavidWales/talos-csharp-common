﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Talos.Common.State.DependencyInjection
{
    public class Container : IContainer
    {
        private readonly List<IDependencyCollection> _dependencyCollections;

        public Container()
        {
            _dependencyCollections = new List<IDependencyCollection>();
        }

        public void Add<T>(T obj)
        {
            var typedCollection = GetOrCreateCollection<T>();
            typedCollection.Register(provider => obj);
        }

        public void Add<T>(Func<IProvider, T> getValue)
        {
            var typedCollection = GetOrCreateCollection<T>();
            typedCollection.Register(getValue);
        }

        public void AddSingleton<T>(T obj)
        {
            var typedCollection = GetOrCreateCollection<T>();
            typedCollection.Register(provider => obj, isSingleton: true);
        }

        public void AddSingleton<T>(Func<IProvider, T> getValue)
        {
            var typedCollection = GetOrCreateCollection<T>();
            typedCollection.Register(getValue, isSingleton: true);
        }

        public virtual IProvider BuildProvider()
        {
            return new Provider(_dependencyCollections);
        }

        public int Count<T>()
        {
            var typedCollection = GetOrCreateCollection<T>();
            return typedCollection.Count();
        }

        private DependencyCollection<T> GetOrCreateCollection<T>()
        {
            var collection = _dependencyCollections.FirstOrDefault(x => x.Type == typeof(T));
            if (collection is null)
            {
                collection = new DependencyCollection<T>();
                _dependencyCollections.Add(collection);
            }

            return (DependencyCollection<T>)collection;
        }
    }
}
