﻿namespace Talos.Common.State.Forge
{
    public interface IForgeListener<T> where T: IForgeState
    {

        void BindForge(T state);

    }
}
