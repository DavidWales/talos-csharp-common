﻿using System;
using System.Collections.Generic;
using Talos.Common.DesignPatterns.Singleton;

namespace Talos.Common.State.Forge
{
    public class Forge<T> : Singleton<Forge<T>> where T: IForgeState, new()
    {

        private List<IForgeListener<T>> _forgeListeners;
        private T _state;

        public Forge()
        {
            _forgeListeners = new List<IForgeListener<T>>();
            _state = new T();
        }

        public static void RegisterForgeListener(IForgeListener<T> forgeListener)
        {
            var instance = GetInstance();
            instance._forgeListeners.Add(forgeListener);
        }

        public static void SetState(Action<T> setter)
        {
            var instance = GetInstance();
            setter(instance._state);
            NotifyForgeListeners();
        }

        public static void NotifyForgeListeners()
        {
            var instance = GetInstance();
            foreach(var listener in instance._forgeListeners)
            {
                listener.BindForge(instance._state);
            }
        }
    }
}
