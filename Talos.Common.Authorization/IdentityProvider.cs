﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talos.Common.Authorization
{
    public class IdentityProvider
    {

        public string ClientName { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public AuthorizationType AuthorizationType { get; set; }

    }
}
