﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talos.Common.Authorization
{
    public interface IAuthorizationEngine
    {

        string Authorize(List<IdentityProvider> identityProviders, string clientId);

    }
}
