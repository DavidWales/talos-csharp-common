﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talos.Common.Authorization.OAuth
{
    public interface IFlow
    {

        string Authorize(OAuthIdentityProvider identityProvider);

    }
}
