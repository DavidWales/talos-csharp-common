﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talos.Common.Authorization.OAuth.AuthorizationCode
{
    public class AuthorizationCodeIdentityProvider : OAuthIdentityProvider
    {

        public string AuthorizationCodeEndpoint { get; set; }

        public string AccessTokenEndpoint { get; set; }

    }
}
