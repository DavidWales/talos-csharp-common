﻿using System.Collections.Generic;

namespace Talos.Common.Authorization.OAuth
{
    public class OAuthIdentityProvider : IdentityProvider
    {

        public OAuthFlow Flow { get; set; }

        public List<string> RedirectUris { get; set; }

    }
}
