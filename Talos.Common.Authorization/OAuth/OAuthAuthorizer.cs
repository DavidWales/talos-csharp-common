﻿using System;
using System.Collections.Generic;
using System.Text;
using Talos.Common.Authorization.OAuth.AuthorizationCode;

namespace Talos.Common.Authorization.OAuth
{
    public class OAuthAuthorizer : IAuthorizer
    {

        public Dictionary<OAuthFlow, IFlow> FlowMappings;

        public OAuthAuthorizer()
        {
            FlowMappings = new Dictionary<OAuthFlow, IFlow>();
            RegisterFlowMappings();
        }

        protected virtual void RegisterFlowMappings()
        {
            FlowMappings.Add(OAuthFlow.AUTHENTICATION_CODE, new AuthorizationCodeFlow());
        }

        public string Authorize(IdentityProvider identityProvider)
        {
            OAuthIdentityProvider oAuthIdentityProvider = identityProvider as OAuthIdentityProvider;

            IFlow flow = FlowMappings[oAuthIdentityProvider.Flow];
            if(flow == null)
            {
                throw new KeyNotFoundException("There is no flow registered to the given flow type.");
            }

            return flow.Authorize(oAuthIdentityProvider);
        }

    }
}
