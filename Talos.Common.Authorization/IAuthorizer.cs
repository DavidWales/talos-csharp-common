﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talos.Common.Authorization
{
    public interface IAuthorizer
    {

        string Authorize(IdentityProvider identityProvider);

    }
}
