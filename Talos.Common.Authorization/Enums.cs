﻿using System.ComponentModel;

namespace Talos.Common.Authorization
{

    public enum AuthorizationType
    {
        [Description("OAuth")]
        OAUTH = 0,

        [Description("SAML")]
        SAML = 1
    }

    public enum OAuthFlow
    {
        [Description("Authentication Code")]
        AUTHENTICATION_CODE = 0,

        [Description("Password")]
        PASSWORD = 1
    }

}
