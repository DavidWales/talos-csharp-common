﻿using System.Collections.Generic;
using System.Linq;
using Talos.Common.Authorization.OAuth;

namespace Talos.Common.Authorization
{
    public class AuthorizationEngine : IAuthorizationEngine
    {

        protected Dictionary<AuthorizationType, IAuthorizer> AuthorizationTypeMapping;

        public AuthorizationEngine()
        {
            AuthorizationTypeMapping = new Dictionary<AuthorizationType, IAuthorizer>();
            RegisterAuthorizationTypes();
        }

        protected virtual void RegisterAuthorizationTypes()
        {
            AuthorizationTypeMapping.Add(AuthorizationType.OAUTH, new OAuthAuthorizer());
        }

        public string Authorize(List<IdentityProvider> identityProviders, string clientId)
        {
            IdentityProvider identityProvider = identityProviders.FirstOrDefault(p => p.ClientId == clientId);
            if(identityProvider == null)
            {
                throw new KeyNotFoundException("No identity provider could be found with the given client id.");
            }

            IAuthorizer authorizer = AuthorizationTypeMapping[identityProvider.AuthorizationType];
            if(authorizer == null)
            {
                throw new KeyNotFoundException("There is no registered authorizer for the found identity provider.");
            }

            return authorizer.Authorize(identityProvider);
        }

    }
}
