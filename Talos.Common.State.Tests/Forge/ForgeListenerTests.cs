﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Talos.Common.State.Forge;

namespace Talos.Common.State.Tests.Forge
{
    [TestClass]
    public class ForgeListenerTests : BaseTest
    {

        [TestMethod]
        [DynamicData(nameof(ForgeTestsData.RegisteredListenerNotifiedData), typeof(ForgeTestsData))]
        public void RegisteredListenerNotified(string strValue, int counterValue)
        {
            var forgeListener = GivenRegisteredListener();
            Forge<ForgeTestState>.SetState(state =>
            {
                state.TestStr = strValue;
                state.Counter = counterValue;
            });

            forgeListener.TestStr.Should().Be(strValue);
            forgeListener.Counter.Should().Be(counterValue);
        }

        [TestMethod]
        [DynamicData(nameof(ForgeTestsData.UnregisteredListenerNotNotifiedData), typeof(ForgeTestsData))]
        public void UnregisteredListenerNotNotified(string strValue, int counterValue)
        {
            var forgeListener = GivenUnregisteredListener();
            Forge<ForgeTestState>.SetState(state =>
            {
                state.TestStr = strValue;
                state.Counter = counterValue;
            });

            forgeListener.TestStr.Should().NotBe(strValue);
            forgeListener.Counter.Should().NotBe(counterValue);
        }
    }
}
