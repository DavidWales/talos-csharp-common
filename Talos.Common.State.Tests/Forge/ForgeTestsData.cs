﻿using System.Collections.Generic;

namespace Talos.Common.State.Tests.Forge
{
    public static class ForgeTestsData
    {

        public static IEnumerable<object[]> RegisteredListenerNotifiedData
        {
            get
            {
                yield return new object[]
                {
                    "Test",
                    4
                };
            }
        }

        public static IEnumerable<object[]> UnregisteredListenerNotNotifiedData
        {
            get
            {
                yield return new object[]
                {
                    "Test",
                    4
                };
            }
        }
    }
}
