﻿using Talos.Common.State.Forge;

namespace Talos.Common.State.Tests.Forge
{
    public class ForgeTestListener : IForgeListener<ForgeTestState>
    {
        public string TestStr;
        public int? Counter;

        public void BindForge(ForgeTestState state)
        {
            TestStr = state.TestStr;
            Counter = state.Counter;
        }
    }
}
