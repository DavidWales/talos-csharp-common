﻿using Talos.Common.State.Forge;

namespace Talos.Common.State.Tests.Forge
{
    public class ForgeTestState : IForgeState
    {

        public string TestStr;

        public int? Counter;

    }
}
