﻿using Talos.Common.State.Forge;
using Talos.Common.State.Tests.Forge;

namespace Talos.Common.State.Tests
{
    public class BaseTest
    {

        protected ForgeTestListener GivenRegisteredListener()
        {
            var forgeListener = new ForgeTestListener
            {
                TestStr = null,
                Counter = null
            };
            Forge<ForgeTestState>.RegisterForgeListener(forgeListener);
            return forgeListener;
        }

        protected ForgeTestListener GivenUnregisteredListener()
        {
            return new ForgeTestListener
            {
                TestStr = null,
                Counter = null
            };
        }
    }
}
