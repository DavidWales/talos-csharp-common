﻿using System.Collections.Generic;
using Talos.Common.State.DependencyInjection;

namespace Talos.Common.State.Tests.DependencyInjection
{
    public static class ProviderTestData
    {
        public static IEnumerable<object[]> GetsSomeListOfDependenciesData
        {
            get
            {
                var container = new Container();
                container.Add(new EmptyDependency());
                container.Add(new EmptyDependency());
                var provider = container.BuildProvider();

                yield return new object[]
                {
                    provider,
                    2
                };

                var delayedContainer = new Container();
                delayedContainer.Add(p => new EmptyDependency());
                delayedContainer.Add(p => new EmptyDependency());
                delayedContainer.Add(p => new EmptyDependency());
                var delayedProvider = delayedContainer.BuildProvider();

                yield return new object[]
                {
                    delayedProvider,
                    3
                };
            }
        }

        public static IEnumerable<object[]> GetsNoneListOfDependenciesData
        {
            get
            {
                var container = new Container();
                var provider = container.BuildProvider();

                yield return new object[]
                {
                    provider
                };

                var delayedContainer = new Container();
                var delayedProvider = delayedContainer.BuildProvider();

                yield return new object[]
                {
                    delayedProvider
                };
            }
        }

        public static IEnumerable<object[]> GetSingletonReferenceData
        {
            get
            {
                var container = new Container();
                container.AddSingleton(new EmptyDependency());
                var provider = container.BuildProvider();

                yield return new object[]
                {
                    provider
                };

                var delayedContainer = new Container();
                delayedContainer.AddSingleton(p => new EmptyDependency());
                var delayedProvider = delayedContainer.BuildProvider();

                yield return new object[]
                {
                    delayedProvider
                };
            }
        }

        public static IEnumerable<object[]> GetSingletonValueData
        {
            get
            {
                var container = new Container();
                container.AddSingleton(5);
                var provider = container.BuildProvider();

                yield return new object[]
                {
                    provider
                };

                var delayedContainer = new Container();
                delayedContainer.AddSingleton(p => 5);
                var delayedProvider = delayedContainer.BuildProvider();

                yield return new object[]
                {
                    delayedProvider
                };
            }
        }
    }
}
