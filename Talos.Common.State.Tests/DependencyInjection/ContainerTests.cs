﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Talos.Common.State.DependencyInjection;

namespace Talos.Common.State.Tests.DependencyInjection
{
    [TestClass]
    public class ContainerTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(ContainerTestData.CanAddListOfDependenciesData), typeof(ContainerTestData))]
        public void CanAddListOfDependencies(List<EmptyDependency> dependencies)
        {
            var container = new Container();
            foreach(var x in dependencies)
            {
                container.Add(x);
            }

            container.Count<EmptyDependency>().Should().Be(dependencies.Count);
        }

        [TestMethod]
        [DynamicData(nameof(ContainerTestData.CanAddSingletonData), typeof(ContainerTestData))]
        public void CanAddSingleton(List<EmptyDependency> dependencies)
        {
            var container = new Container();
            foreach (var x in dependencies)
            {
                container.AddSingleton(x);
            }

            container.Count<EmptyDependency>().Should().Be(1);
        }

        [TestMethod]
        [DynamicData(nameof(ContainerTestData.CanAddDelayedListOfDependenciesData), typeof(ContainerTestData))]
        public void CanAddDelayedListOfDependencies(List<EmptyDependency> dependencies)
        {
            var container = new Container();
            foreach (var x in dependencies)
            {
                Func<IProvider, EmptyDependency> delayedEvaluation = provider => x;
                container.Add(delayedEvaluation);
            }

            container.Count<EmptyDependency>().Should().Be(dependencies.Count);
        }

        [TestMethod]
        [DynamicData(nameof(ContainerTestData.CanAddDelayedSingletonData), typeof(ContainerTestData))]
        public void CanAddDelayedSingleton(List<EmptyDependency> dependencies)
        {
            var container = new Container();
            foreach (var x in dependencies)
            {
                Func<IProvider, EmptyDependency> delayedEvaluation = provider => x;
                container.AddSingleton(delayedEvaluation);
            }

            container.Count<EmptyDependency>().Should().Be(1);
        }

        [TestMethod]
        [DynamicData(nameof(ContainerTestData.CanBuildProviderData), typeof(ContainerTestData))]
        public void CanBuildProvider(List<EmptyDependency> dependencices)
        {
            var container = new Container();
            foreach (var x in dependencices)
            {
                container.Add(x);
            }

            var provider = container.BuildProvider();
            provider.Should().NotBeNull();
        }
    }
}
