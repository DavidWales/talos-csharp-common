﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Talos.Common.State.Tests.DependencyInjection
{
    public static class ContainerTestData
    {
        public static IEnumerable<object[]> CanAddListOfDependenciesData
        {
            get
            {
                yield return new object[]
                {
                    new List<EmptyDependency>()
                    {
                        new EmptyDependency(),
                        new EmptyDependency()
                    }
                };
            }
        }

        public static IEnumerable<object[]> CanAddSingletonData
        {
            get
            {
                yield return new object[]
                {
                    new List<EmptyDependency>()
                    {
                        new EmptyDependency(),
                        new EmptyDependency()
                    }
                };
            }
        }

        public static IEnumerable<object[]> CanAddDelayedListOfDependenciesData
        {
            get
            {
                yield return new object[]
                {
                    new List<EmptyDependency>()
                    {
                        new EmptyDependency(),
                        new EmptyDependency()
                    }
                };
            }
        }

        public static IEnumerable<object[]> CanAddDelayedSingletonData
        {
            get
            {
                yield return new object[]
                {
                    new List<EmptyDependency>()
                    {
                        new EmptyDependency(),
                        new EmptyDependency()
                    }
                };
            }
        }

        public static IEnumerable<object[]> CanBuildProviderData
        {
            get
            {
                yield return new object[]
                {
                    new List<EmptyDependency>()
                    {
                        new EmptyDependency(),
                        new EmptyDependency()
                    }
                };
            }
        }
    }
}
