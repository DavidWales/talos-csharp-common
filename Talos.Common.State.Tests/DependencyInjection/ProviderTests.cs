﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using Talos.Common.State.DependencyInjection;

namespace Talos.Common.State.Tests.DependencyInjection
{
    [TestClass]
    public class ProviderTests : BaseTest
    {
        [TestMethod]
        [DynamicData(nameof(ProviderTestData.GetsSomeListOfDependenciesData), typeof(ProviderTestData))]
        public void GetsSomeListOfDependencies(IProvider provider, int expectedCount)
        {
            var emptyDependenciesOption = provider.Get<EmptyDependency>();
            emptyDependenciesOption.Should().NotBeNull();
            emptyDependenciesOption.IsSome().Should().BeTrue();

            var emptyDependencies = emptyDependenciesOption.UnwrapOr(new List<EmptyDependency>());
            emptyDependencies.Count().Should().Be(expectedCount);
        }

        [TestMethod]
        [DynamicData(nameof(ProviderTestData.GetsNoneListOfDependenciesData), typeof(ProviderTestData))]
        public void GetsNoneListOfDependencies(IProvider provider)
        {
            var emptyDependenciesOption = provider.Get<EmptyDependency>();
            emptyDependenciesOption.Should().NotBeNull();
            emptyDependenciesOption.IsNone().Should().BeTrue();
        }

        [TestMethod]
        [DynamicData(nameof(ProviderTestData.GetSingletonReferenceData), typeof(ProviderTestData))]
        public void GetSingletonReference(IProvider provider)
        {
            var emptyDependencyOption = provider.GetSingletonReference<EmptyDependency>();

            emptyDependencyOption.Should().NotBeNull();
            emptyDependencyOption.IsSome().Should().BeTrue();
        }

        [TestMethod]
        [DynamicData(nameof(ProviderTestData.GetSingletonValueData), typeof(ProviderTestData))]
        public void GetSingletonValue(IProvider provider)
        {
            var emptyDependency = provider.GetSingletonValue<int>();
            emptyDependency.Should().NotBe(default);
        }
    }
}
