﻿using System.Numerics;
using Talos.Common.Numerics.BigIntegers.RandomGenerator;

namespace Talos.Common.Numerics.BigIntegers.PrimeCheckers
{
    /// <summary>
    /// Primality Test based on the Miller-Rabin method.
    /// https://en.wikipedia.org/wiki/Miller%E2%80%93Rabin_primality_test
    /// </summary>
    public class MillerRabinPrimeChecker : IPrimeChecker
    {
        private readonly IRandomBigIntegerGenerator _randomBigIntegerGenerator;
        private readonly int _iterations;

        public MillerRabinPrimeChecker(IRandomBigIntegerGenerator randomBigIntegerGenerator,
            int iterations = 5)
        {
            _randomBigIntegerGenerator = randomBigIntegerGenerator;
            _iterations = iterations;
        }

        public bool IsPrime(BigInteger n)
        {
            (var d, var r) = FindParameters(n);
            for (int i = 0; i < _iterations; i++)
            {
                var canWitnessPrimality = CanWitnessPrimality(n, d, r);
                if (canWitnessPrimality) continue;

                return false;
            }
            return true;
        }

        private (BigInteger, BigInteger) FindParameters(BigInteger n)
        {
            BigInteger d = n - 1;
            BigInteger r = 0;

            while ((d & 1) == 0)
            {
                d >>= 1;
                r++;
            }

            return (d, r);
        }

        private bool CanWitnessPrimality(BigInteger n, BigInteger d, BigInteger r)
        {
            var x = _randomBigIntegerGenerator.Next(2, n - 2);
            var lemma = BigInteger.ModPow(x, d, n);
            if (lemma == 1 || lemma == n - 1) return true;

            for (int j = 0; j < r - 1; j++)
            {
                lemma = BigInteger.ModPow(lemma, 2, n);
                if (lemma == n - 1) return true;
            }

            return false;
        }
    }
}
