﻿using System.Numerics;

namespace Talos.Common.Numerics.BigIntegers.PrimeCheckers
{
    public class NaivePrimeChecker : IPrimeChecker
    {
        public bool IsPrime(BigInteger n)
        {
            if (n <= 1) return false;
            if (n <= 3) return true;

            if (n % 2 == 0 || n % 3 == 0) return false;
            for (BigInteger i = 5; i * i <= n; i += 6)
            {
                if (n % i == 0 || n % (i + 2) == 0)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
