﻿using System.Numerics;

namespace Talos.Common.Numerics.BigIntegers.PrimeCheckers
{
    public interface IPrimeChecker
    {

        bool IsPrime(BigInteger n);

    }
}
