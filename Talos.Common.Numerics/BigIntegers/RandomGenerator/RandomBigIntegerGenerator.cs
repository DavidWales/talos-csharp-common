﻿using System;
using System.Numerics;

namespace Talos.Common.Numerics.BigIntegers.RandomGenerator
{
    public class RandomBigIntegerGenerator : IRandomBigIntegerGenerator
    {
        private readonly Random _random;

        public RandomBigIntegerGenerator()
        {
            _random = new Random();
        }

        public BigInteger Next(BigInteger min, BigInteger max)
        {
            if(min > max)
            {
                var tmp = min;
                min = max;
                max = tmp;
            }

            var numBits = max.ToByteArray().Length * 8;
            while(true)
            {
                var option = Next(numBits);
                if(option >= min && option <= max)
                {
                    return option;
                }
            }
        }

        public BigInteger Next(int numBits)
        {
            var byteBuffer = new byte[numBits / 8];
            _random.NextBytes(byteBuffer);
            return new BigInteger(byteBuffer);
        }
    }
}
