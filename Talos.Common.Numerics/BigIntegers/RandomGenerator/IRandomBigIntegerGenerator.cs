﻿using System.Numerics;

namespace Talos.Common.Numerics.BigIntegers.RandomGenerator
{
    public interface IRandomBigIntegerGenerator
    {

        BigInteger Next(BigInteger min, BigInteger max);

        BigInteger Next(int numBits);

    }
}
