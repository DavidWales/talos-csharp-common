﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public class StrategyContext<T> : IStrategyContext<T>
    {

        private readonly IStrategy<T> _strategy;
        
        public StrategyContext(IStrategy<T> strategy)
        {
            _strategy = strategy;
        }

        public void Execute(T parameters)
        {
            _strategy.Execute(parameters);
        }
    }
}
