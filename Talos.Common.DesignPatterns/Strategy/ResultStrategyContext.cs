﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public class StrategyContext<TParameter, TResult> : IStrategyContext<TParameter, TResult>
    {

        private readonly IStrategy<TParameter, TResult> _strategy;
        
        public StrategyContext(IStrategy<TParameter, TResult> strategy)
        {
            _strategy = strategy;
        }

        public TResult Execute(TParameter parameters)
        {
            return _strategy.Execute(parameters);
        }
    }
}
