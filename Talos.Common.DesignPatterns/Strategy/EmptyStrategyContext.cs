﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public class StrategyContext : IStrategyContext
    {

        private readonly IStrategy _strategy;
        
        public StrategyContext(IStrategy strategy)
        {
            _strategy = strategy;
        }

        public void Execute()
        {
            _strategy.Execute();
        }
    }
}
