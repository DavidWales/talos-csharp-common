﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public interface IStrategy<T>
    {

        void Execute(T parameters);

    }
}
