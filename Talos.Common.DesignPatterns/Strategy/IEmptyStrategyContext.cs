﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public interface IStrategyContext
    {

        void Execute();

    }
}
