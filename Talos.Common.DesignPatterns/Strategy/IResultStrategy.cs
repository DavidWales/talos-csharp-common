﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public interface IStrategy<TParameter, TResult>
    {

        TResult Execute(TParameter parameters);

    }
}
