﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public interface IStrategyContext<T>
    {

        void Execute(T parameters);

    }
}
