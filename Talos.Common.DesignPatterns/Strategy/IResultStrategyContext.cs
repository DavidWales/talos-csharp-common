﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public interface IStrategyContext<TParameter, TResult>
    {

        TResult Execute(TParameter parameters);

    }
}
