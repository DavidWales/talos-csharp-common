﻿namespace Talos.Common.DesignPatterns.Strategy
{
    public interface IStrategy
    {

        void Execute();

    }
}
