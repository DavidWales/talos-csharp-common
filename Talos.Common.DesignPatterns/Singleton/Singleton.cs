﻿namespace Talos.Common.DesignPatterns.Singleton
{
    public class Singleton<T> : ISingleton<T> where T: new()
    {

        private static T _instance { get; set; }

        public static T GetInstance()
        {
            if(_instance == null)
            {
                _instance = new T();
            }
            return _instance;
        }

    }
}
