﻿namespace Talos.Common.DesignPatterns.State
{
    public interface IState<T>
    {

        IStateContext<T> Context { get; set; }

        void Execute(T parameters = default);

    }
}
