﻿namespace Talos.Common.DesignPatterns.State
{
    public interface IState
    {

        IStateContext Context { get; set; }

        void Execute();

    }
}
