﻿namespace Talos.Common.DesignPatterns.State
{
    public interface IStateContext 
    {

        IState State { get; set; }

        void Execute();

    }
}
