﻿namespace Talos.Common.DesignPatterns.State
{
    public class StateContext<T> : IStateContext<T>
    {

        private IState<T> _state;

        public IState<T> State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
                _state.Context = this;
            }
        }

        public virtual void Execute(T parameters)
        {
            State?.Execute(parameters);
        }

    }
}
