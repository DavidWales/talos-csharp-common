﻿namespace Talos.Common.DesignPatterns.State
{
    public class StateContext : IStateContext
    {

        private IState _state;

        public IState State
        {
            get
            {
                return _state;
            }
            set
            {
                _state = value;
                _state.Context = this;
            }
        }

        public virtual void Execute()
        {
            State?.Execute();
        }

    }
}
