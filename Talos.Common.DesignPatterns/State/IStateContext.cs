﻿namespace Talos.Common.DesignPatterns.State
{
    public interface IStateContext<T> 
    {

        IState<T> State { get; set; }

        void Execute(T parameters);

    }
}
