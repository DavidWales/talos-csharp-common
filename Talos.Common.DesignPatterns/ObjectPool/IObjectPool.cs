﻿namespace Talos.Common.DesignPatterns.ObjectPool
{
    public interface IObjectPool<T> where T : new()
    {

        void WarmUp(int count);

        void Destroy(T obj);

        T Create();

        int Count();

    }
}
