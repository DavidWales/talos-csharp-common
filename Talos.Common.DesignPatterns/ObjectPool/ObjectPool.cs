﻿using System.Collections.Generic;
using System.Linq;

namespace Talos.Common.DesignPatterns.ObjectPool
{
    public class ObjectPool<T> : IObjectPool<T> where T : new()
    {

        private List<T> _objectPool;

        public ObjectPool()
        {
            _objectPool = new List<T>();
        }

        public ObjectPool(int initialCount)
        {
            _objectPool = new List<T>();
            WarmUp(initialCount);
        }

        public virtual void WarmUp(int count)
        {
            for(int i = _objectPool.Count; i < count; i++)
            {
                var newObj = new T();
                _objectPool.Add(newObj);
            }
        }

        public virtual void Destroy(T obj)
        {
            _objectPool.Add(obj);
        }

        public virtual T Create()
        {
            return GetOrCreate();
        }

        public virtual int Count()
        {
            return _objectPool.Count;
        }

        private T GetOrCreate()
        {
            if(_objectPool.Count > 0)
            {
                var firstObj = _objectPool.First();
                _objectPool.RemoveAt(0);
                return firstObj;
            }
            return new T();
        }

    }
}
