﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Talos.Common.DesignPatterns.Memento
{
    public class BinaryMomento<T> : IMemento<T>
    {

        public T State { get; set; }
        public T SavedState { get; private set; }

        private readonly BinaryFormatter _binaryFormatter;

        public BinaryMomento()
        {
            _binaryFormatter = new BinaryFormatter();
        }

        public virtual void SaveToMemento()
        {
            using (var memorySream = new MemoryStream())
            {
                _binaryFormatter.Serialize(memorySream, State);
                memorySream.Position = 0;
                SavedState = (T)_binaryFormatter.Deserialize(memorySream);
            }
        }

        public virtual void RestoreFromMemento()
        {
            State = SavedState;
        }

    }
}
