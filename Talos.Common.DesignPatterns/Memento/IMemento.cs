﻿namespace Talos.Common.DesignPatterns.Memento
{
    public interface IMemento<T>
    {

        T State { get; set; }
        T SavedState { get; }

        void RestoreFromMemento();

        void SaveToMemento();

    }
}
