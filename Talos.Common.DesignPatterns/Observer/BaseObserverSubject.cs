﻿using System.Collections.Generic;

namespace Talos.Common.DesignPatterns.Observer
{
    public abstract class BaseObserverSubject : IObserverSubject<BaseObserverSubject>
    {

        private List<IObserver<BaseObserverSubject>> _observers;

        public BaseObserverSubject()
        {
            _observers = new List<IObserver<BaseObserverSubject>>();
        }

        public virtual void AttachObserver(IObserver<BaseObserverSubject> observer)
        {
            if (!_observers.Contains(observer))
            {
                _observers.Add(observer);
            }
        }

        public virtual void DetachObserver(IObserver<BaseObserverSubject> observer)
        {
            if (_observers.Contains(observer))
            {
                _observers.Remove(observer);
            }
        }

        public virtual void NotifyObservers()
        {
            foreach(var observer in _observers)
            {
                observer?.Handle(this);
            }
        }

        public virtual int ObserverCount()
        {
            return _observers.Count;
        }
    }
}
