﻿namespace Talos.Common.DesignPatterns.Observer
{
    public interface IObserverSubject<T>
    {

        int ObserverCount();

        void NotifyObservers();

        void AttachObserver(IObserver<T> observer);

        void DetachObserver(IObserver<T> observer);

    }
}
