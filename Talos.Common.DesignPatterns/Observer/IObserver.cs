﻿namespace Talos.Common.DesignPatterns.Observer
{
    public interface IObserver<T>
    {

        void Handle(T subject);

    }
}
