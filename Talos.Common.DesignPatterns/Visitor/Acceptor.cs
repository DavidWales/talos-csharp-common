﻿namespace Talos.Common.DesignPatterns.Visitor
{
    public abstract class Acceptor : IAcceptor<Acceptor>
    {
        public void Accept(IVisitor<Acceptor> visitor)
        {
            visitor.Visit(this);
        }
    }
}
