﻿namespace Talos.Common.DesignPatterns.Visitor
{
    public interface IAcceptor<T> where T : IAcceptor<T>
    {
        void Accept(IVisitor<T> visitor);
    }
}
