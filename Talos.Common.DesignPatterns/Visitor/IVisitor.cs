﻿namespace Talos.Common.DesignPatterns.Visitor
{
    public interface IVisitor<T> where T : IAcceptor<T>
    {
        void Visit(T obj);
    }
}
