﻿namespace Talos.Common.DesignPatterns.Command
{
    public abstract class BaseCommand<T> : ICommand
    {

        public T Parameter { get; private set; }

        public BaseCommand(T parameter)
        {
            Parameter = parameter;
        }

        public abstract void Execute();

    }
}
