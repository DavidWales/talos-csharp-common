﻿namespace Talos.Common.DesignPatterns.Command
{
    public abstract class BaseCommand : ICommand
    {

        public BaseCommand()
        { }

        public abstract void Execute();

    }
}
