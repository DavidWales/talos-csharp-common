﻿namespace Talos.Common.DesignPatterns.Command
{
    public interface ICommand
    {

        void Execute();

    }
}
